from pathlib import Path
from setuptools import setup, find_packages

setup(
    name='gameoflife',
    version='0.1',
    description="Conway's Gameo of Life",
    author='Henry S. Harrison',
    author_email='henry.harrison@kogniasports.com',
    long_description=open('README.md').read(),

    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[path.stem for path in Path('src').glob('*.py')],
    zip_safe=True,
    include_package_data=True,

    install_requires=[
        'numpy == 1.18.4',
        'scipy == 1.4.1',
    ],
    extras_require=dict(
        test=[
            'coverage == 5.1',
            'flake8 == 3.8.2',
            'mccabe == 0.6.1',
            'mypy == 0.770',
            'pyflakes == 2.2.0',
            'pytest == 5.4.2',
            'pytest-cov == 2.9.0',
            'pytest-flake8 == 1.0.6',
            'pytest-mypy == 0.6.2',
            'pydocstyle == 5.0.2',
            'pep8-naming == 0.10.0',
            'pycodestyle == 2.6.0',
            'pytest-pydocstyle==2.1.3',
        ],
    ),

    entry_points=dict(
        console_scripts=[
            'gameoflife = gameoflife:main',
        ],
    ),
)
