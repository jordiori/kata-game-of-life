"""Game of life."""
import re
import sys
from typing import List, TextIO

import numpy as np
from scipy.signal import convolve2d


def increment_state(state: List[List[bool]]) -> List[List[bool]]:
    """Increment game of life state.

    Updates the game state according to the game of life rules.

    Parameters
    ----------
    state : list of list of bool
        The game state, in dense format as a 2D array, with `True` representing alive cells.


    Returns
    -------
    list of list of bool
        The next state, in dense format.

    """
    if len(set(len(row) for row in state)) != 1:
        raise TypeError

    state_array = np.asarray(state)
    neighbors = _n_neighbors(state_array)

    live_cell_status = (neighbors >= 2) & (neighbors <= 3)
    dead_cell_status = neighbors == 3
    new_state = (state_array & live_cell_status) | (~state_array & dead_cell_status)

    return new_state.tolist()


def _n_neighbors(state_array: np.ndarray) -> np.ndarray:
    """Calculate the number of living neighbors of each cell.

    Parameters
    ----------
    state_array : array
        The game state, as a 2D numpy array.

    Returns
    -------
    array
        An integer array counting the number of neighbors of each cell.

    """
    mask = np.array([
        [1, 1, 1],
        [1, 0, 1],
        [1, 1, 1],
    ])
    return convolve2d(state_array, mask, mode='same')


_STR_TO_BOOL_REPR = {
    '.': False,
    'O': True,
}
_BOOL_TO_STR_REPR = {v: k for k, v in _STR_TO_BOOL_REPR.items()}


def _deserialize_state(file: TextIO = sys.stdin) -> List[List[bool]]:
    """Deserialize game state from a file or stream.

    Parameters
    ----------
    file : TextIO, optional
        File object. Defaults to standard input.

    Returns
    -------
    list of list of bool

    Raises
    ------
    ValueError
        If the input is invalid.

    """
    lines: List[List[bool]] = []
    for i, line in enumerate(file):
        if line.startswith('!'):
            if lines:
                raise ValueError('Pattern file contains ! after the grid has started.')
            continue
        if not re.match(r'[\.O]+\n', line):
            raise ValueError(f'Invalid pattern row {i}: {line!r}')
        lines.append([_STR_TO_BOOL_REPR[char] for char in line.strip()])
    return lines


def _serialize_state(state: List[List[bool]]) -> str:
    """Serialize game state to a string.

    Parameters
    ----------
    state: list of list of bool

    Returns
    -------
    str

    """
    return '\n'.join(
        ''.join(_BOOL_TO_STR_REPR[value] for value in row)
        for row in state
    ) + '\n'


def main() -> None:
    """Entry point."""
    state = _deserialize_state()
    next_state = increment_state(state)
    sys.stdout.write(_serialize_state(next_state))


if __name__ == '__main__':
    main()
