"""End-to-end tests of gameoflife."""
from pathlib import Path
from subprocess import CalledProcessError, check_output
from typing import List

import pytest


def read_test_pattern_file(name: str, header: bool = True) -> str:
    """Read test pattern.

    Parameters
    ----------
    name : str
        Name of the pattern. It must exist at tests/patterns/{name}.life.
    header : bool, optional
        Whether to keep the header lines starting with ! at the top of the file.

    Returns
    -------
    str
        Contents of the file.

    """
    lines: List[str] = []
    with (Path('tests') / 'patterns' / f'{name}.life').open() as file:
        for line in file:
            if line.startswith('!') and not header:
                continue
            lines.append(line)
    return ''.join(lines)


def run_gameoflife_cli(input_lines: str) -> str:
    """Run gameoflife in subprocess.

    Parameters
    ----------
    input_data : str
        Input state, as a multi-line string.

    Returns
    -------
    str
        Output state, as a multi-line string.

    """
    return check_output('gameoflife', input=input_lines, universal_newlines=True)


def test_gameoflife_runs_correctly():
    """If gameoflife runs with the toad pattern, it should return the toad-prime pattern."""
    pattern = read_test_pattern_file('toad')
    assert run_gameoflife_cli(pattern) == read_test_pattern_file('toad-prime', header=False)


def test_gameoflife_errors_with_invalid_file_no_pattern():
    """If the input is invalid, gameoflife should exit with an error code."""
    pattern = read_test_pattern_file('invalid-no-pattern')
    with pytest.raises(CalledProcessError):
        run_gameoflife_cli(pattern)


def test_gameoflife_errors_with_invalid_file_no_grid():
    """If the input is not a grid, gameoflife should exit with an error code."""
    pattern = read_test_pattern_file('invalid-not-grid')
    with pytest.raises(CalledProcessError):
        run_gameoflife_cli(pattern)


def test_gameoflife_errors_with_invalid_file_header_in_middle():
    """If the input contains a ! row in the middle, gameoflife should exit with an error code."""
    pattern = read_test_pattern_file('invalid-header-in-middle')
    with pytest.raises(CalledProcessError):
        run_gameoflife_cli(pattern)


def test_gameoflife_errors_with_invalid_file_bad_row():
    """If an input row contains a wrong character, gameoflife should exit with an error code."""
    pattern = read_test_pattern_file('invalid-bad-row')
    with pytest.raises(CalledProcessError):
        run_gameoflife_cli(pattern)
